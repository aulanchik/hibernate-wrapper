package com.reborne.SmartHibernateConnector;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.reborne.SmartHibernateConnector.utils.IHibernateConnector;

public class App {

	private static String hibernateStatus = "";
	
	private static Logger LOGGER = LoggerFactory.getLogger(App.class);
	
	public static void main( String[] args ) {
    
		IHibernateConnector hibernateConnector = HibernateFactory.getHibernateConnector(hibernateStatus);
		
		LOGGER.info("Currently Selected " + hibernateConnector.getClass().getSimpleName() + " configuration.");
		
	}
}
