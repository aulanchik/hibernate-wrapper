package com.reborne.SmartHibernateConnector.utils;

import org.hibernate.HibernateException;
import org.hibernate.Session;

public interface IHibernateConnector {

	HibWrapper openSession() throws HibernateException;
	
	Session getOrCreateSession() throws HibernateException;
	
	void reconnect() throws HibernateException;
}