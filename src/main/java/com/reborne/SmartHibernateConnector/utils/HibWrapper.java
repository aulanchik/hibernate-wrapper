package com.reborne.SmartHibernateConnector.utils;

import java.sql.SQLException;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.Transaction;

import com.reborne.SmartHibernateConnector.entities.BaseEntity;

public class HibWrapper implements AutoCloseable {

	private Session session;
	private boolean close;
	
	public HibWrapper(Session session, boolean close) {
		this.session = session;
		this.close = close;
	}

	public void close() throws Exception {
		if(close) {
			session.close();
		}
	}
	
	public <T extends BaseEntity> void save(T entity) {
		session.save(entity);
	}
	
	public <T extends BaseEntity> void save(List<T> entityList) {
		for (T entity : entityList) {
			session.save(entity);
		}
		
	}

	public <T extends BaseEntity> void delete(T entity) {
		session.delete(entity);
	}
	
	public <T extends BaseEntity> void delete(List<T> entityList) {
		for (T entity : entityList) {
			session.delete(entity);
		}
	}
	
	public <T extends BaseEntity> void saveOrUpdate(T entity) {
		session.saveOrUpdate(entity);
	}
	
	public <T extends BaseEntity> void saveOrUpdate(List<T> entityList) {
		session.saveOrUpdate(entityList);
	}
	
	public Transaction beginTransaction() throws SQLException {
		return session.beginTransaction();
	}
	
	public Criteria createCriteria(Class<? extends BaseEntity> clazz) throws SQLException {
		return session.createCriteria(clazz);
	}
	
}
