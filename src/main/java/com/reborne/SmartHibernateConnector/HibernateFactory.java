package com.reborne.SmartHibernateConnector;

import com.reborne.SmartHibernateConnector.utils.IHibernateConnector;
import com.reborne.SmartHibernateConnector.utils.LiveHibernateConnector;
import com.reborne.SmartHibernateConnector.utils.TestHibernateConnector;

public final class HibernateFactory {

	public static IHibernateConnector getHibernateConnector(String hibernateStatus) {
		if (hibernateStatus == null) {
			return null;
		} else if (hibernateStatus.equalsIgnoreCase("LIVE")) {
			return new LiveHibernateConnector();		
		} else if (hibernateStatus.equalsIgnoreCase("TEST")) {
			return new TestHibernateConnector();
		}else{
			return new TestHibernateConnector();
		}
	}
	
}
